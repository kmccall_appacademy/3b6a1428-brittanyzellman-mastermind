require 'byebug'

class Code

  attr_reader :pegs
  PEGS = {red: "R", green: "G", blue: "B", yellow: "Y", orange: "O",
          purple: "P", }

  def initialize(key)
    @pegs = key
  end

  def self.parse(string)
    # debugger
    guess = []
    string.each_char { |ch| guess << ch.upcase }
    guess.each {|el| raise "invalid guess" if !PEGS.values.include?(el)}
    Code.new(guess)
  end

  def self.random
    # debugger
    colors = PEGS.values
    key = []
    4.times do
      key << colors[rand(7)]
    end
    Code.new(key)
  end

  def [](key)
    @pegs[key]
  end

  def ==(code)
    if code.is_a?(Code)
      self.pegs == code.pegs
    else
      false
    end
  end

  def exact_matches(guess)
    matches = 0
    4.times do |el|
      matches += 1 if self[el] == guess[el]
    end
    matches
  end

  def near_matches(guess)
    matches = 0
    g = guess.pegs
    s = self.pegs
    # iterate through each of the guess pegs, if the self pegs contains
    # that peg and the index doesnt match increment matches and change
    # self[i] to downcase so it doesn't get recounted.
    g.each_with_index do |el, i|
      if s.include?(el) && s.index(el) != i
        matches += 1
        s[s.index(el)] = el.downcase
      end
    end
    # take away perfect matches
    i = 0
    while i < 4
      if guess.pegs[i] == self.pegs[i]
        matches -= 1
      end
      i += 1
    end
    matches
  end

end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "Enter a guess: "
    guess = gets.chomp
    Code.parse(guess)
  end

  def display_matches(guess)
    near = @secret_code.near_matches(guess)
    exact = @secret_code.exact_matches(guess)
    puts "You have #{exact} exact matches and #{near} near matches"
  end



end
